console.log(window)

const form = document.getElementById('get-detail')
const detailContainer = document.getElementById('show-detail')
const detailBox = document.getElementById('detail')
const email = document.getElementById('my-email')
const pass = document.getElementById('my-pass')
const sex = document.getElementById('sex')
const btn = document.getElementById('btn')



const validateEmailAndPass = (email, pass)=>{
    if (email === '' || pass === ''){
        alert("All Feilds are required")
        return false
    }else{
        if (email.includes('@')){
            //Email verified
            const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$/
        
            if (pass.match(regex) && pass === pass.match(regex)[0]){
                return true
            }else{
                alert("Password Should Contain an Upercase, a lowercase and a digit")
            }
        }else{
            alert("Enter Correct Email")
            return false
        }
    } 
}

const validateRole = ()=>{
    const role = document.querySelectorAll('.role')
    for (let x of role){
        if (x.checked){
            return x.val
        }
    }
    return ''
}

const validatePerm = ()=>{
    const perm = document.querySelectorAll('.perm')
    const grantedPerm = []
    for (let x of perm){
        if (x.checked){
            grantedPerm.push(x.value)
        }
    }
    if (grantedPerm.length >= 2){
        return grantedPerm
    }else{
        return []
    }
}

const displayDetail = (email, pass, sex, role, perm)=>{
    form.style.display = 'none'
    detailContainer.style.display = 'box'
    detailBox.innerHTML = `
        <h1>Please Confirm Once</h1>
        <p>Email : ${email}</p>
        <p>Password : ${pass}</p>
        <p>Sex: ${sex}</p>
        <p>Role : ${role}</p>
        <p>Permission</p>
    `        
    //Display all permission
    const permDisp = document.createElement('ul')
    for (x of perm){
        const selPerm = document.createElement('li')
        selPerm.innerText = x
        permDisp.appendChild(selPerm)
    }
    detailBox.appendChild(permDisp)

    //To enter a button at end
    const confirm = document.createElement('button')
    confirm.setAttribute('type', 'submit')
    confirm.innerText = 'Confirm'
    detailContainer.appendChild(confirm)

}

btn.addEventListener('click', (e)=>{
    e.preventDefault()
    if(validateEmailAndPass(email.value, pass.value)){
        //check if role is selected
        const userRole = validateRole()
        //check if two or more perm is selected
        const userPerm = validatePerm()
        if (userRole === ''){
            alert("Please Select a Role")
        }else{
            //validated role
            if (userPerm.length === 0){
                alert("Please Select at least 2 Permission")
            }else{
                // validated permission 
                displayDetail(email.value, pass.value,sex.value, userRole, userPerm)
            }
        }
    }
})